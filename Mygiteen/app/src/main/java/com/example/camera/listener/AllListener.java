package com.example.camera.listener;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraDevice;
import android.view.TextureView;

import androidx.annotation.NonNull;

import com.example.camera.activity.TempActivity;


/***
 * TextureView.SurfaceTextureListener:当与此文理关联的表面可用时，可以使用监听器通知
 *
 */
public class AllListener extends CameraDevice.StateCallback implements TextureView.SurfaceTextureListener {

    TempActivity tempActivity;
    public AllListener(Activity activity){
        tempActivity =(TempActivity) activity;
    }
    /** 相机监听器**/
    @Override
    public void onOpened(@NonNull CameraDevice camera) {
       tempActivity.mCameraDevice = camera;
       tempActivity.createCameraPreviewSession();
    }

    @Override
    public void onDisconnected(@NonNull CameraDevice camera) {
        camera.close();
        tempActivity.mCameraDevice = null;
    }

    @Override
    public void onError(@NonNull CameraDevice camera, int error) {
        camera.close();
        tempActivity.mCameraDevice = null;
        tempActivity.finish();
    }

    /** 当TextureView追备好使用SurfaceTexture是调用**/
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

        tempActivity.openCamera(width,height);
    }
    /**SurfaceTexture的缓冲区大小改变是调用**/
    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }
    /**当指定SurfaceTexture对象将被回收是调用**/
    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }
    /**SurfaceTexture通过更新制定的值是调用**/
    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
