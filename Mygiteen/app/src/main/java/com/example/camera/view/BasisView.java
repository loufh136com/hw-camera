package com.example.camera.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class BasisView extends View {

    public BasisView(Context context){
        super(context);
    }

    public BasisView(Context context, AttributeSet atts){
        super(context, atts);
    }

    public BasisView(Context context, AttributeSet atts, int defStyle){
        super(context,atts,defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(50);
        canvas.drawCircle(200,200,150,paint);
    }
}
